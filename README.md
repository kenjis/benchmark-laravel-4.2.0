# Laravle 4.2.1 for Benchmarking

## Installation

~~~
$ mkdir ~/tmp
$ cd ~/tmp
$ git clone git@bitbucket.org:kenjis/benchmark-laravel-4.2.0.git
$ cd benchmark-laravel-4.2.0
$ composer install
~~~

~~~
$ chmod o+w app/storage/*
~~~

~~~
$ cd /path/to/htdocs
$ ln -s ~/tmp/benchmark-laravel-4.2.0/public/ lara
~~~

## Benchmarking

~~~
$ siege -b -c 10 -t 3S http://localhost/lara/hello?name=BEAR
~~~
