<?php

class HelloController extends BaseController {
	public function getIndex()
	{
		$name = Input::get('name', 'World');
		return 'Hello ' . $name . '!';
	}
}
